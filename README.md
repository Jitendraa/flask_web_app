# StackOverflow

This project imitates the working of popular question and answer platform [stackoverflow](https://stackoverflow.com/)

## Project tree structure.
    .
    ├── all_database_schema.md
    ├── api_Flask Web Project part-1.pdf
    ├── app
    │   ├── __init__.py
    │   ├── models.py
    │   └── routes.py
    ├── app.db
    ├── config.py
    ├── Flask Web Project part-1.postman_collection.json
    ├── README.md
    └── run.py

## Requirements
- python 3.6+
- Flask 1.1.1
- SQLite DB browser (not necessary)
## Setting up

1. It is recommended to use a virtual environment, if you don't wish to use a virtual environment proceed to step 3

    `python3 -m venv <your-vitual-environment-name>`
    or 
    `conda create --name <your-vitual-environment-name>`

2. Use your virtual environment

    `source <your-vitual-environment-name>/bin/activate`
    
    **Note:** To deactivate from virtual environment `deactivate`
    
3. Intall the pip requirements for the project

    `pip install -r requirements.txt`

## Running the project

- To run the project, type the following command

    `python run.py`

- By default the host is your localhost, port is set to 5000, and runs in debug mode.

- ENV_FILE_LOCATION - ENV_FILE_LOCATION is the environment variable which is stored .env file relative to __int__.py.
The .env file contains JWT_SECRET_KEY 

## note:
   To set this value mac/linux can run the command: export ENV_FILE_LOCATION=./.env


## Using the APIs

- This project currently has only the APIs for imitating stackoverflow
- The APIs perform [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) on the [database](all_database_schema.md)
- Read [here](api_documentation.pdf) on how to use the APIs.
- If want to read the API documentation on Postman, import the [Flask Web Project part-1.postman_collection.json].