'''
It conatins the configuration of the whole project.
SQLite database is used in this project.

classes:
    Config
'''
import os
BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    '''
    The class contians the information of database connectivity.
    '''
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access']
    JWT_TOKEN_LOCATION = 'cookies'    # comment it during postman.
    JWT_COOKIE_CSRF_PROTECT = False
    JWT_CSRF_CHECK_FORM = True

    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    SEARCH_INDEX = 'questions'
    POST_PER_PAGE = 25


HOST = '127.0.0.1'
PORT = '5000'
DOMAIN = "http://" + HOST + ':' + PORT
