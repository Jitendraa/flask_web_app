# StackOverflow Database Schema

# Tables
 - User
 - Question
 - Answer

---
## Table: User
| column | data type | description |
--------------------------------------------------------------------------------
| user_id | Integer (primary key) | Primary key for a user record. |
| username | String (unique) | The name of the user that is for auhtorization and authentication.
| email | string (unique) | The email id of the user. |
| password-hash | string (hashed) | The hash of password of a user. |
| questions | db.relationship('Question', backref='username')| all questions posted by a user.
---

## Table: Question
| column | data type | description |
----------------------------------------------------------------------------------
| question_id | int (primary key) | Primary key for a question record.|
| title | string | The title of the question. |
| description | string | The question explained in detail. |
| user_id | int (Foreign key: Users.user_id) | Foreign key on Users.user_id for linking each question to a user. |
answers | db.relationship('Answer', backref='question', lazy='dynamic') | all answers of a questions
---

## Table: Answer
| column | data type | description |
---------------------------------------------------------------------------------------
| answer_id | int (primary key) | Primary key for a questions record.|
| answer_body | string(not null) | The answer submitted by a user. |
| user_id | int (Foreign key: Users.user_id) | Foreign key to Users.user_id for linking each question to a user. |
| question_id | int (Foreign key: Questions.question_id) | Foreign key on Questions.question_id for linking each answer to a question.|
----------------------------------------------------------------------------------------