'''
This module is responsible for excuting the application.
'''
from app import app, db
from app.models import User, Question, Answer


@app.shell_context_processor
def make_shell_context():
    '''
    The app.shell_context_processor decorator registers the function
    as a shell context function.

    When the flask shell command runs, it will invoke this function and
    register the items returned by it in the shell session.

    returns:
        dict: dictionary of all modules.
    '''
    return {
        'db': db,
        'User': User,
        'Question': Question,
        'Answer': Answer
    }


app.run(debug=True)
