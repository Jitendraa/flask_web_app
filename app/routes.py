'''
The module contains all the realted Api's of the project and
its related functions.

functions:
    index()
    signup()
    login()
    logout()
    change_password()
    delete user(username) -> string
    get_all_question()
    search_question(question_id) -> int
    post_question()
    edit_posted_question(question_id) -> int
    delete_question(question_id) -> int
    search_all_answers_of_question(question_id) -> int
    search_specific_ans_of_question(question_id, answer_id) -> int, int
    post_answer(question_id) -> int
    edit_posted_answer(question_id, answer_id) -> int, int
    delete_posted_answer(question_id, answer_id) -> int, int
'''

import datetime
from flask import jsonify, request, Response, redirect
from flask_jwt_extended import create_access_token, jwt_required, \
    get_jwt_identity, get_raw_jwt
from app import app, db, blacklist
from app.models import User, Question, Answer
from app.search import search_query_index, add_questions_to_index, \
    remove_questions_from_index, delete_index, remove_from_index, \
    add_to_index

# User related operations.
@app.route('/api/signup', methods=['POST'])
def signup():
    '''
    The funcion deals with the registration of new user.

    returns:
        dict: contains user_id and username of the user.
    '''
    data = request.get_json()
    user = User(**data)
    user.hash_password()
    exiting_user = User.query.filter_by(email=user.email).first()

    if not exiting_user:
        db.session.add(user)
        db.session.commit()
        return {'id': user.user_id, 'username': user.username}
    else:
        return {"error": "Username/email already exists."}, 409


@app.route('/api/users/<int:user_id>', methods=['PUT'])
@jwt_required
def edit_profile_api(user_id):
    '''
    Modifies the details of the an exiting user.

    Parmeters:
        user_id (int): User id of an exitin user.
    returns:
        Message (dict): A successful message for updating the profile.
                        or
                        Unsccessful messages.
    '''
    try:
        user = User.query.filter_by(user_id=int(get_jwt_identity())).first()
        logged_in_user_id = int(get_jwt_identity())
        if logged_in_user_id == user_id:
            data = request.get_json()
            user.username = data['username']
            user.email = data['email']
            user.password_hash = data['password_hash']
            user.hash_password()
            db.session.commit()
            return {'Message': 'Updated successfully.'}, 200
        else:
            return {'Message': "Unauthorised user"}, 201
    except Exception as e:
        print(e)
        return {'error': "IntegrityError or keyError"}, 404


@app.route('/api/login', methods=['POST'])
def login():
    '''
    Handles POST request to login as a user and issue jwt token.

    returns:
        dict: contains generated token of logged in user.
    '''
    try:
        login_data = request.get_json()
        user = User.query.filter_by(username=login_data['username']).first()
        authorized = user.check_password(login_data['password_hash'])
        if not user:
            return {'error': 'Invalid user'}, 401
        if not authorized:
            return {'error': "Invalid Password"}, 401

        expires = datetime.timedelta(days=7)
        access_token = create_access_token(
            identity=str(user.user_id), expires_delta=expires
        )
        return {'token': access_token}, 200
    except ValueError:
        return {"error": "Please enter username and password"}, 500
    except TypeError:
        return {"error": "data is not in the josn format."}
    except AttributeError:
        return {"error": "Invalid username/password."}, 500
    except:
        return {'error': "something went wrong."}, 404


@app.route('/api/logout', methods=['DELETE'])
@jwt_required
def logout():
    '''
    A logged in user can raise a delete request for logout

    returns:
        dict: contains succesfull logout message.
    '''
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)

    return {'Message': "logged out successfully!"}, 200


@app.route('/user/<int:user_id>/password', methods=['PUT'])
@jwt_required
def change_password(user_id):
    '''
    A logged in user can raise a PUT requst to change password.

    returns:
        dict: constains message 'passsword changed succesfully.'
    '''
    try:
        user = User.query.filter_by(user_id=int(get_jwt_identity())).first()
        logged_in_user_id = int(get_jwt_identity())
        if logged_in_user_id == user_id:
            data = request.get_json()
            user.password_hash = data['password_hash']
            user.hash_password()
            db.session.commit()
            return {'Message': 'Password changed successfully.'}, 200
        else:
            return {'Message': "Unauthorised user"}, 201
    except KeyError:
        return {'error': "KeyError"}, 500


@app.route('/api/users/<int:user_id>', methods=['DELETE'])
@jwt_required
def delete_user_account_api(user_id):
    '''
    A logged in user can raise a request to delete his account.

    returns:
        dict: contains successfull deletion message.
    '''
    try:
        login_user = User.query.filter_by(user_id=int(get_jwt_identity())).first()
        current_user = User.query.get(user_id)

        if current_user is None:
            return {'error': 'Not found!'}, 404
        if login_user is current_user:
            db.session.delete(current_user)
            db.session.commit()
            logout()

            return {"message": "user deleted successfully!"}, 200
        else:
            return {"message: ": "You are not the owner of the account"}, 500
    except:
        db.session.rollback()
        return {"errro": "Somthing went wrong!"}, 404


@app.route('/api/users/<int:user_id>')
@jwt_required
def get_user_details_api(user_id):
    '''
    Get user details of an exiting user.

    Parmeters:
        user_id (int): User id of an exiting user.
    returns:
        response (dict): contains all the answers detials of a qeustion.
    '''
    user = User.query.get(user_id)
    if user is None:
        return {"error": "Resource not found"}, 404

    user = User.query.get(user_id)
    data = {}
    data['user_id'] = user.user_id
    data['username'] = user.username
    data['email'] = user.email
    data['password_hash'] = user.password_hash

    return jsonify(data)


# curd operations on Question database
@app.route('/api/questions', methods=['GET'])
def get_all_question():
    '''
    Any user can raise a GET request to get all posted questions.

    returns:
        response : JSON form of all questions on the board.
    '''
    questions = Question.query.all()
    response = [question.json_format() for question in questions]
    try:
        delete_index()
    except:
        pass
    add_questions_to_index()

    return jsonify(response)


@app.route('/api/questions/<question_id>', methods=['GET'])
def search_question_api(question_id):
    '''
    Any user can raise a GET request to get a specific question.

    parameters:
        question_id (int): unique id of a specific question.

    returns:
        dict : contains details of a quried quesiton.
    '''
    question = Question.query.get(question_id)
    if question is None:
        return {'error': 'question not found'}, 404

    return question.json_format()


@app.route('/api/questions', methods=['POST'])
@jwt_required
def post_question():
    '''
    A logged in user can raise a POST request to post question.

    returns:
        dict: contains successful posted message.
    '''
    try:
        data = request.get_json()
        logged_in_user_id = get_jwt_identity()
        question = Question(**data, user_id=logged_in_user_id)
        db.session.add(question)
        db.session.commit()
        add_to_index(question)

        return redirect(f'/api/questions/{question.question_id}/answers')
    except KeyError:
        db.session.rollback()
        return {'error', 'KeyError'}, 400
    except TypeError:
        return {'error', "Please check input data! or something went wrong!"}


@app.route('/api/users/<int:user_id>/questions/<int:question_id>', methods=['PUT'])
@jwt_required
def edit_question_api(user_id, question_id):
    '''
    Only a registered user can raise a PUT request to updata a question.

    returns:
        dict : contaions edited message or key error
               or unauthorized user message.
    '''
    try:
        question = Question.query.get(question_id)
        if question is None:
            return {'error': 'qeustion not found!'}, 404
        if question.user_id == user_id:
            data = request.get_json()
            question.title = data['title']
            question.description = data['description']
            db.session.commit()
            return redirect('/api/questions')
            return {'message': 'edited successfully.'}, 200
        else:
            return {'error': "you are not authorized user of this question."}, 201
    except KeyError:
        return {'error': "key error"}
    except TypeError:
        return {'error': "Please check input data! or something went wrong!"}


@app.route('/api/users/<int:user_id>/questions/<int:question_id>', methods=['DELETE'])
@jwt_required
def delete_question_api(user_id, question_id):
    '''
    A registered user can raise a DELETE reques to remove a posted question.

    parameters:
        question_id (int): Question id of a question to be deleted.

    returns:
        dict: Successful deletion message or unauthorized user message.
    '''
    question = Question.query.get(question_id)
    if question is None:
        return {'error': "Question not found!"}, 404

    if question.user_id == user_id:
        answers = list(question.answers)
        delete_list = [question] + answers

        for item in delete_list:
            db.session.delete(item)
        db.session.commit()
        remove_from_index(question)
        return {'message': 'Quesiton deleted successfully.'}, 200
    else:
        return {'error': 'unauthorized user.'}, 201

# Curd operaton on Answer database
@app.route('/api/questions/<question_id>/answers', methods=['GET'])
def search_all_answers_of_question(question_id):
    '''
    Get all posted answers of a question by raising a GET request.

    parameters:
        question_id (int): id a question to which get all answers.

    returns:
        response (dict): contains all the answers detials of a qeustion.
    '''
    question = Question.query.get(question_id)
    if question is None:
        return {"error": 'question not found!'}
    answers = question.answers
    response = [ans.json_format() for ans in answers]

    return jsonify(response)


@app.route('/api/users/<int:user_id>/questions', methods=['GET'])
@jwt_required
def get_all_question_by_user(user_id):
    '''
    Get all the questions posted by the user.

    Parameters:
        user_id (int): User id of an exiting user.
    returns:
        response (dict): contains all the answers detials of a qeustion.
    '''
    user = User.query.get(user_id)
    if user is None:
        return {"error": "Resource not found!"}, 404
    questions = user.questions
    questions = [question.json_format() for question in questions]

    return jsonify(questions)


@app.route('/api/users/<int:user_id>/answers')
@jwt_required
def get_all_answers_by_user(user_id):
    '''
    Get all the answer posted by a registered user.

    Parameters:
        user_id (int): User id of a registered user.
    returns:
        response (dict): contains all the answers detials of a qeustion.
    '''
    user = User.query.get(user_id)
    if user is None:
        return {"error": "Resource not found!"}, 404
    answers = user.answers
    answers = [answer.json_format() for answer in answers]

    return jsonify(answers)


@app.route('/api/questions/<question_id>/answers/<answer_id>', methods=['GET'])
def search_specific_ans_of_question(question_id, answer_id):
    '''
    Search a specific answer of question by raising GET request.

    parameters:
        quesion_id (int): id of a question.
        answer_id (int): id of answer of a question.

    returns:
        answer (dic): details of the answer of a question.

    '''
    question = Question.query.get(question_id)
    answer = Answer.query.get(answer_id)

    if (question is None) or (answer is None):
        return {'error': 'Not found'}, 404

    return answer.json_format()


@app.route('/api/questions/<question_id>/answers', methods=['POST'])
@jwt_required
def post_answer_api(question_id):
    '''
    A registered user can add an answer to a posted question.

    parameter:
        qeustion_id (int): id of a question to be answered.
    '''
    logged_in_user = User.query.filter_by(user_id=int(get_jwt_identity())).first()
    if not logged_in_user:
        return {'error': "unauthorised access"}, 201

    question = Question.query.get(question_id)
    if question is None:
        return {'error': 'Question not found.'}, 404

    answer_data = request.get_json()
    try:
        answer = answer_data['answer_body']
        if answer:
            added_answer = Answer(
                answer_body=answer,
                question_id=question_id,
                user_id=logged_in_user.user_id
            )
            db.session.add(added_answer)
            db.session.commit()
            remove_questions_from_index()
            add_questions_to_index()
            return {'message': "answer added successfully of  question."}, 200
    except KeyError:
        return {'error': 'KeyError'}, 500
    except TypeError:
        return {'error': "Type error! insert answer in correct format."}, 401


@app.route('/api/questions/<question_id>/answers/<answer_id>', methods=['PUT'])
@jwt_required
def edit_answer_api(question_id, answer_id):
    '''
    Edit a posted answer by an authorized answer.

    parameters:
        question_id (int): id of the question which answer is to be edited.
        answer_id (int): id of answer to be edited.

    returns:
        dict: succesfull message or error message.
    '''
    try:
        question = Question.query.get(question_id)
        answer = Answer.query.get(answer_id)
        if (question is None) or (answer is None):
            return {'error': "Resource not found."}, 404
        else:
            data = request.get_json()
            logged_in_user_id = int(get_jwt_identity())

            if answer.user_id == logged_in_user_id:
                answer.answer_body = data['answer_body']
                db.session.add(answer)
                db.session.commit()
                remove_questions_from_index()
                add_questions_to_index()
                return {"message": "Answer updated."}, 200
            else:
                return {'error': "Please login to answer!"}, 201
    except KeyError:
        return {"error": "Key Error"}, 400
    except TypeError:
        return {'error': "Type error! Enter edited data in correct format."}, 501


@app.route('/api/users/<int:user_id>/answers/<int:answer_id>', methods=['DELETE'])
@jwt_required
def delete_answer_api(user_id, answer_id):
    '''
    Delete a posted answer by an authorized answer.

    parameters:
        question_id (int): id of the question which answer is to be deleted.
        answer_id (int): id of answer to be deleted.

    returns:
        dict: succesfull message or error message.
    '''
    answer = Answer.query.get(answer_id)

    if (user_id is None) or (answer is None):
        return {'error': "Resorce not found!"}, 404
    else:
        if user_id == answer.user_id:
            db.session.delete(answer)
            db.session.commit()
            remove_questions_from_index()
            add_questions_to_index()
            return {'Message': "Answer deleted successfully!"}, 200
        else:
            return {"error": "Unauthorised user!"}, 201


@app.route('/api/search')
def search_api():
    '''
    Searches a query

    returns:
        response (dict): contains all the answers detials of a qeustion.
        or
        dict : An successful message in error form.
    '''
    try:
        query = request.args.get('query')
        if query:
            ids, total_ids = search_query_index(
                app.config['SEARCH_INDEX'], query, 1, 100
            )
        response = list()
        for id in ids:
            question = Question.query.get(id)
            response.append(question.json_format())
        return jsonify(response)
    except Exception as exception:
        print('error: ', exception)
        return {"eror": "something went wrong"}, 500
