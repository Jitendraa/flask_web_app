'''
Initialization of the application and all important modules of
flask are imported.
'''
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from config import Config
from elasticsearch import Elasticsearch


app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']]) \
    if app.config['ELASTICSEARCH_URL'] else None


app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
jwt = JWTManager(app)
login = LoginManager(app)

blacklist = set()

from app import routes, models, views
