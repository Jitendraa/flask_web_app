'''
The module contains all the functin related to elasticsearch.

functions:
    add_to_index
    add_questions_to_index
    remove_from_index
    remove_questions_from_index
    search_query_index
    delete_index
'''
from app import app
from app.models import Question

INDEX = app.config['SEARCH_INDEX']


def add_to_index(model, index=INDEX):
    '''
    Adds a question to the index.

    parameters:
        model (obj): An object of a table.
        index ('str'): Name of the index.
    '''
    if not app.elasticsearch:
        return

    payload = model.get_searchable_fields()
    app.elasticsearch.index(
        index, doc_type=index, id=model.question_id, body=payload
    )


def add_questions_to_index(index=INDEX):
    '''
    Add all the question to the index.

    Parameters:
        index (str): Name of an index.
    '''
    for question in Question.query.all():
        print(question)
        add_to_index(question)


def remove_from_index(model, index=INDEX):
    '''
    Removes a question from an exiting index.

    Parameters:
        model (obj): An object of a table.
        index (str): Name of a index.
    '''
    if not app.elasticsearch:
        return
    app.elasticsearch.delete(
        index=index, doc_type=index, id=model.question_id
    )


def remove_questions_from_index(index=INDEX):
    '''
    Removes all questions from the INDEX

    Parameters:
        index (str): Name of an INDEX.
    '''
    for question in Question.query.all():
        remove_from_index(index, model=question)


def search_query_index(index, query, page, per_page):
    '''
    Searches a query from an exiting index.

    Parameters:
        index (str): Name of an index.
        query (str): A query to search from an exiting index.
        page (int): A page number.
        per_page: The number of elements present in a page.
    '''
    if not app.elasticsearch:
        return [], 0
    search_result = app.elasticsearch.search(
        index=index, doc_type=index,
        body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
              'from': (page - 1) * per_page, 'size': per_page})
    ids = [int(hit['_id']) for hit in search_result['hits']['hits']]

    return ids, search_result['hits']['total']


def delete_index(index=INDEX):
    '''
    Deletes an exiting index.

    Parameters:
        index (index): An index name.
    '''
    app.elasticsearch.indices.delete('index')
