'''
This model contains view functions for the web pages which takes the information
by requesting to the backend API functions and render the response as web pages.
'''

import requests
from flask import render_template, request, redirect, url_for, flash, json, make_response
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, set_access_cookies,
    set_refresh_cookies, unset_jwt_cookies,
    verify_jwt_in_request_optional,
    get_raw_jwt
)
from app import app, jwt, blacklist
from config import DOMAIN
from app.forms import LoginForm, RegistrationForm, QuestionForm


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    '''
    The function checks weather the logged in token is present in
    the blacklist or not.

    Parameters:
        decryped_token (string): An unreadble format.
    returns:
        bool: a boolean value either True or False.
    '''
    jti = decrypted_token['jti']
    return jti in blacklist


def get_logged_in_user():
    '''
    Returns user ID of a user who is logged in.

    returns:
        user_id (int): user ID of a logged in user.
    '''
    try:
        verify_jwt_in_request_optional()
        return get_jwt_identity()
    except  Exception as e:
        print(e)
        pass

# user
@app.route('/signin', methods=['GET', 'POST'])
def user_login():
    '''
    The function is used for authentication for a user during log in.
    It handles the GET and POST requests coming from client site.
    The access cookies are storedon the client-side upon response from
    the server.
    '''
    current_user = get_logged_in_user()
    if current_user:
        return redirect(url_for('index'))

    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        form_data = dict(request.form.items())
        payload = dict()
        payload['username'] = form_data['username']
        payload['password_hash'] = form_data['password']
        url = 'http://127.0.0.1:5000/api/login'
        headers = {'Content-type': 'application/json'}
        api_response = requests.post(url, headers=headers, data=json.dumps(payload))

        if api_response.status_code == 500:
            flash('Invalid username or password.')
            return redirect(url_for('user_login'))

        elif api_response.status_code == 200:
            access_token = api_response.json()['token']
            url = url_for('index')
            new_response = make_response(redirect(url, 302))
            set_access_cookies(new_response, access_token)

            return new_response
    return render_template("login.html", title='Sign In', form=form)


# @jwt_required
@app.route('/logout', methods=['GET'])
def user_logout():
    '''
    The function deals with the GET request for log out a user.
    The access is deleted from the client site.
    '''
    url = DOMAIN + '/api/logout'
    user_cookies = request.cookies
    api_response = requests.delete(url, cookies=user_cookies)

    if api_response.status_code in [200, 401]:
        web_response = make_response(redirect('/signin', 302))
        unset_jwt_cookies(web_response)
        return web_response

    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def user_signup():
    '''
    The function deals with the POST request to register a user
    on the website.
    '''
    form = RegistrationForm()
    if request.method == 'POST':
        if request.form['password'] != request.form['confirm']:
            flash("Please recheck entered passworlds!")
            return render_template('signup.html', title='Sign UP', form=form)

        url = DOMAIN + url_for('signup')
        payload = {}
        payload['username'] = request.form['username']
        payload['email'] = request.form['email']
        payload['password_hash'] = request.form['password']
        headers = {'Content-type': 'application/json'}
        response = requests.post(url, headers=headers, data=json.dumps(payload))

        if response.status_code == 409:
            return render_template('signup.html', error=response.text, form=form)
        return redirect(url_for('user_login'))
    else:
        return render_template('signup.html', title='Sign Up', form=form)


@app.route('/')
def index():
    '''
    The function deals with the GET request and renders home page
    of the website.
    '''
    info = requests.get('http://127.0.0.1:5000/api/questions').json()
    user_id = get_logged_in_user()

    return render_template(
        'index.html', info=info, logged_in_user_id=user_id,
        user_id=user_id
    )


@app.route('/questions', methods=['GET', 'POST'])
@jwt_required
def ask_question():
    '''
    The function manages to deal with GET request to render ask question
    page and POST request get the data from the API to get all question
    details.
    '''
    post_form = QuestionForm()
    user_id = get_jwt_identity()

    if request.method == 'POST':
        url = DOMAIN + '/api/questions'
        payload = {}
        payload['title'] = request.form['title']
        headers = dict(request.headers)
        payload['description'] = request.form['description']
        headers['Content-type'] = 'application/json'
        api_response = requests.post(
            url, headers=headers, data=json.dumps(payload)
        )
        if api_response.status_code in [200, 302, 405]:
            return redirect(url_for('index'))
    return render_template(
        'post-question.html', post_form=post_form, logged_in_user_id=user_id
    )


@app.route('/questions/<int:question_id>', methods=['GET'])
def question_details(question_id):
    '''
    The function handles GET request from the client-site and hits the API
    to get all question details.
    '''
    user_id = get_logged_in_user()
    question = requests.get(f'http://127.0.0.1:5000/api/questions/{question_id}').json()
    all_answers = requests.get(f'http://127.0.0.1:5000/api/questions/{question_id}/answers').json()

    return render_template(
        'question-details.html', question=question,
        answers=all_answers, logged_in_user_id=user_id,
        user_id=user_id
    )


@app.route('/users/<user_id>/edit', methods=['GET', 'POST'])
@jwt_required
def edit_profile(user_id):
    '''
    The function deals with both GET and POST request from the client-site.
    An authorised user is allowed to make changes in his profile
    by POST request which directly hits the edit profile API.
    '''
    user_url = DOMAIN + url_for('get_user_details_api', user_id=user_id)
    headers = dict(request.headers)
    headers['Content-type'] = 'application/json'
    user_data = requests.get(user_url, headers=headers).json()
    print(user_data)

    if request.method == 'POST':
        payload = {}
        payload['username'] = request.form['username']
        payload['email'] = request.form['email']
        payload['password_hash'] = request.form['password']
        print(payload)
        url = DOMAIN + url_for('edit_profile_api', user_id=user_id)
        api_response = requests.put(url, headers=headers, data=json.dumps(payload))

        if api_response.status_code == 200:
            return redirect(url_for('get_user_details', user_id=user_id))
    return render_template(
        'edit-profile.html', user_id=user_id,
        logged_in_user_id=user_id,
        user_data=user_data
    )


@app.route('/users/<int:user_id>/questions/<int:question_id>', methods=['GET', 'POST'])
@jwt_required
def edit_question(user_id, question_id):
    '''
    The function deals with both GET and POST request from the client-site.
    An authorised user is allowed to make changes in his posted question
    by POST request which directly hits the edit question API.
    '''
    question_form = QuestionForm()
    headers = dict(request.headers)
    headers['Content-type'] = 'application/json'
    question_url = DOMAIN + url_for(
        'search_question_api', user_id=user_id, question_id=question_id
    )
    question = requests.get(question_url).json()
    if request.method == 'POST':
        payload = {}
        payload['title'] = request.form['title']
        payload['description'] = request.form['description']
        edit_url = DOMAIN + url_for(
            'edit_question_api', user_id=user_id, question_id=question_id
        )
        api_response = requests.put(
            edit_url, headers=headers, data=json.dumps(payload)
        )
        if api_response.status_code == 200:
            return redirect(url_for('get_user_details', user_id=user_id))

    return render_template(
        'edit-question.html', question=question,
        question_form=question_form, logged_in_user_id=user_id
    )


@app.route('/users/<user_id>', methods=['GET'])
@jwt_required
def get_user_details(user_id):
    '''
    Handles the GET request from client-site by the owner of profile to get his
    whole information given by him. And renders user details webpage.
    '''
    data_api_url = DOMAIN + url_for('get_user_details_api', user_id=user_id)
    questions_api_url = DOMAIN + url_for('get_all_question_by_user', user_id=user_id)
    answers_api_url = DOMAIN + url_for('get_all_answers_by_user', user_id=user_id)
    headers = dict(request.headers)
    headers['Content-type'] = 'application/json'
    user_data = requests.get(data_api_url, headers=headers).json()
    questions_data = requests.get(questions_api_url, headers=headers).json()
    answers_data = requests.get(answers_api_url, headers=headers).json()

    return render_template(
        'user-details.html',
        logged_in_user_id=user_id,
        user_data=user_data,
        questions_data=questions_data,
        answers_data=answers_data
    )


@app.route('/user/<int:user_id>/question/<int:question_id>', methods=['POST'])
@jwt_required
def delete_question(user_id, question_id):
    '''
    The POST request to delete a question by an authorized user. DELETE
    request is made by delete question API to the database. And redirects
    to user details page ohterwiese redirects to the home page on succesfull
    status code obtained from the user.
    '''
    url = DOMAIN + url_for('delete_question_api', user_id=user_id, question_id=question_id)
    headers = dict(request.headers)
    api_response = requests.delete(url, headers=headers)

    if api_response.status_code == 200:
        return redirect(url_for('get_user_details', user_id=user_id))
    else:
        return redirect(url_for('index'))


@app.route('/users/<user_id>/answers/<answer_id>/del', methods=['POST'])
@jwt_required
def delete_answer(user_id, answer_id):
    '''
    The POST request from the client side to delete a answer by hitting the
    delete answer API. And on successful status it returns to user details
    page otherwise to home page.
    '''
    url = DOMAIN + url_for('delete_answer_api', user_id=user_id, answer_id=answer_id)
    headers = dict(request.headers)
    api_response = requests.delete(url, headers=headers)

    if api_response.status_code == 200:
        return redirect(url_for('get_user_details', user_id=user_id))
    return redirect(url_for('index'))


@app.route('/users/<user_id>/del', methods=['POST'])
@jwt_required
def delete_user_account(user_id):
    '''
    Handles with POST request from the client site by an authrised user to delete
    his personal account. On succesful status obtained from the delete user API
    returns redirects to sign up page otherwise home page.
    '''
    url = DOMAIN + url_for('delete_user_account_api', user_id=user_id)
    headers = request.headers
    api_response = requests.delete(url, headers=headers)

    if api_response.status_code == 200:
        user_logout()
        return redirect(url_for('user_signup'))

    return redirect(url_for('index'))


@app.route('/questions/<question_id>/answers/<answer_id>', methods=['GET', 'POST'])
@jwt_required
def edit_answer(question_id, answer_id):
    '''
    Handles with GET request to render edit answer page from the client-site.
    And by POST requst an authorised user can edit his posted question thorough
    edit answer API. On successfulstatus code obtained by the API redirects to
    user details page.
    '''
    user_id = get_jwt_identity()
    ans_url = DOMAIN + url_for(
        'search_specific_ans_of_question',
        question_id=question_id, answer_id=answer_id
    )
    ans_data = requests.get(ans_url).json()

    if request.method == "POST":
        payload = {}
        payload['answer_body'] = request.form['answer_body']
        url = DOMAIN + url_for(
            'edit_answer_api', question_id=question_id, answer_id=answer_id
        )
        headers = dict(request.headers)
        headers['Content-type'] = 'application/json'
        api_response = requests.put(url, headers=headers, data=json.dumps(payload))

        if api_response.status_code == 200:
            flash('Answer updated.')
            return redirect(url_for('get_user_details', user_id=user_id))
    return render_template(
        'edit-answer.html', logged_in_user_id=user_id, ans=ans_data
    )


@app.route('/questions/<question_id>', methods=['POST'])
@jwt_required
def post_answer(question_id):
    '''
    Handles POST request made by an authorise user from client site. A user
    can post an answer and successful status from API redirects to question
    details page otherwise to home page.
    '''
    if request.method == 'POST':
        payload = {}
        payload['answer_body'] = request.form['answer_body']
        headers = dict(request.headers)
        headers['Content-type'] = 'application/json'
        url = DOMAIN + url_for('post_answer_api', question_id=question_id)
        api_response = requests.post(url, headers=headers, data=json.dumps(payload))
        if api_response.status_code == 200:
            return redirect(url_for('question_details', question_id=question_id))
        return redirect(url_for('index'))


@app.route('/search')
def search_query():
    '''
    The function deals with the GET request from client-site by any
    user to search. The function uses a elastic search to search a query.
    On successful status code obtained by the search API rediects to home
    page with its searched contents.
    '''
    query = request.args.get('query')
    url = DOMAIN + url_for('search_api', query=query)
    user_id = get_logged_in_user()

    if query:
        api_response = requests.get(url)
        api_response_data = api_response.json()
        if api_response.status_code in [200, 302]:
            return render_template(
                'index.html', info=api_response_data, logged_in_user_id=user_id,
                user_id=user_id
            )
    else:
        return redirect(url_for('index'))
