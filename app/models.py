'''
The module contains all the models(table) and its related function.

classes:
    User
    Question
    Answer

functions:
    __repr__()
    hash_password()
    check_password(password) -> string
    json_format()
'''

from flask_bcrypt import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db


class User(UserMixin, db.Model):
    '''
    The class contains table information of user.

    attributes:
    ----------
    username: str
        Username of posting a question or posting a answer
    password: str
        password of a user

    methods:
    --------
    hash_password()
    check_password()
    '''
    user_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    questions = db.relationship('Question', backref='username')
    answers = db.relationship('Answer', backref='username')

    def hash_password(self):
        '''
        A password is hashed inot unreadable format.
        '''
        self.password_hash = generate_password_hash(self.password_hash).decode('utf8')

    def check_password(self, password):
        '''
        A unreadable password is hashed to perform operaton for comparision
        between the passwords.

        parameters:
            password (str): password of a user.

        returns:
            bool: True or Fasle
        '''
        return check_password_hash(self.password_hash, password)


class Question(db.Model):
    '''
    The class contains attribute of reation Qeuestion and josn_format function

    methods:
    --------
    json_format()
    '''
    __searchable__ = ['title', 'description']
    question_id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(139), index=True)
    description = db.Column(db.String(500), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'))
    answers = db.relationship(
        'Answer',
        backref='author',
        lazy='dynamic',
        cascade="all, delete-orphan"
    )

    def json_format(self):
        '''
        The function convets the relation into the json format.

        returns:
            dic (dict): contains the json formatted data of the relation.
        '''
        columns = self.__table__.columns.keys()
        dic = {}
        for column in columns:
            column_value = self.__getattribute__(column)
            dic[column] = column_value

        return dic

    def get_searchable_fields(self):
        '''
        '''
        searchable_data = {}
        for field in self.__searchable__:
            searchable_data['field'] = self.__getattribute__(field)

        if self.answers:
            searchable_data['answers'] = list()
            for ans in self.answers:
                searchable_data['answers'].append(ans.answer_body)

        return searchable_data


class Answer(db.Model):
    '''
    The class contians all the attributes of Answer relation.

    methods:
        josn_format()
    '''
    answer_id = db.Column(db.Integer, primary_key=True)
    answer_body = db.Column(db.String(1000), index=True)
    question_id = db.Column(db.Integer, db.ForeignKey('question.question_id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.user_id'))

    def json_format(self):
        '''
        It converts the Answer relation into json fomate.

        returns:
            josn_format_data (dict): contains the joson formatted data
                                     of the relation.
        '''
        columns = self.__table__.columns.keys()
        json_format_data = {}
        for column in columns:
            value = self.__getattribute__(column)
            json_format_data[column] = value

        return json_format_data
